#include "stm32f0xx_hal_conf.h"
#include "gpio.h"

static bool gpio_set_full(Gpio *gpio, GPIO_MODER mode, GPIO_OTYPER otyper, GPIO_PUPDR pup, GPIO_AFSEL af, bool initial_state);
static inline uint8_t gpio_get_index_from_pin(Gpio *gpio)
{
    for (uint8_t index = 0; index < 16; index++) {
        if ((1 << index) == gpio->pin) {
            return index;
        }
    }

    return 0;
}

bool gpio_set_as_input(Gpio *gpio, GPIO_PUPDR pup)
{
    return gpio_set_full(gpio, GPIO_MODER_INPUT, 0, pup, GPIO_AFSEL_AF0, false);
}

bool gpio_set_as_analog(Gpio *gpio)
{
    return gpio_set_full(gpio, GPIO_MODER_ANALOG, 0, 0, GPIO_AFSEL_AF0, false);
}

bool gpio_set_as_output(Gpio *gpio, GPIO_OTYPER otyper, bool initial_state)
{
    return gpio_set_full(gpio, GPIO_MODER_OUTPUT, otyper, 0, GPIO_AFSEL_AF0, initial_state);
}

bool gpio_set_as_alternate_input(Gpio *gpio, GPIO_PUPDR pup, GPIO_AFSEL afsel)
{
    return gpio_set_full(gpio, GPIO_MODER_ALTERNATE, 0, pup, afsel, false);
}

bool gpio_set_as_alternate_output(Gpio *gpio, GPIO_OTYPER otyper, GPIO_AFSEL afsel, bool initial_state)
{
    return gpio_set_full(gpio, GPIO_MODER_ALTERNATE, otyper, 0, afsel, initial_state);
}

static bool gpio_set_full(Gpio *gpio, GPIO_MODER mode, GPIO_OTYPER otyper, GPIO_PUPDR pup, GPIO_AFSEL af, bool initial_state)
{
    uint8_t pos = gpio_get_index_from_pin(gpio);
    if (pos > 15) {
        return false;
    }
    gpio_write_pin(gpio, initial_state);
    gpio->GPIOx->OTYPER &= ~(0x1 << pos);
    gpio->GPIOx->OTYPER |= (otyper << pos);
    gpio->GPIOx->MODER &= ~(0x3 << (pos * 2));
    gpio->GPIOx->MODER |= (mode << (pos * 2));
    gpio->GPIOx->OSPEEDR &= ~(0x3 << (pos * 2));
    gpio->GPIOx->OSPEEDR |= (GPIO_OSPEEDR_HIGH << (pos * 2));
    gpio->GPIOx->PUPDR &= ~(0x3 << (pos * 2));
    gpio->GPIOx->PUPDR |= (pup << (pos * 2));
    gpio->GPIOx->AFR[0] &= ~(0xF << (pos * 4));
    gpio->GPIOx->AFR[0] |= (af << (pos * 2));
    return true;
}

void gpio_enable_isr(Gpio *gpio, GPIO_EDGE edge)
{
    uint8_t pos = gpio_get_index_from_pin(gpio);

    /* Enable SYSCFG Clock */
    __HAL_RCC_SYSCFG_CLK_ENABLE();

    uint32_t icr = SYSCFG->EXTICR[pos >> 2];
    CLEAR_BIT(icr, ((uint32_t)0x0F) << (4 * (pos & 0x03)));
    SET_BIT(icr, (GPIO_GET_INDEX(gpio->GPIOx)) << (4 * (pos & 0x03)));
    SYSCFG->EXTICR[pos >> 2] = icr;

    EXTI->FTSR &= ~(gpio->pin);
    EXTI->RTSR &= ~(gpio->pin);
    EXTI->IMR &= ~(gpio->pin);
    if (edge == GPIO_EDGE_FALLING) {
        EXTI->FTSR |= (gpio->pin);
    } else {
        EXTI->RTSR |= (gpio->pin);
    }

    if (EXTI->PR & gpio->pin) {
        /* clear pending */
        EXTI->PR = gpio->pin;
    }

    EXTI->IMR |= (gpio->pin);
}

void gpio_disable_isr(Gpio *gpio)
{
    EXTI->IMR &= ~(gpio->pin);
    EXTI->RTSR &= ~(gpio->pin);
    EXTI->FTSR &= ~(gpio->pin);
    if (EXTI->PR & gpio->pin) {
        /* clear pending */
        EXTI->PR = gpio->pin;
    }
}

