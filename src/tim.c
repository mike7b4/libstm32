#include "tim.h"
/* FIXME this should not be here and not hardcoded */
#define get_aph1_freq() (12000000)

bool tim_pwm_on(TIM_TypeDef *TIMx, uint8_t channel, uint32_t freq, uint8_t dutycycle)
{
    if (dutycycle > 100)
        dutycycle = 100;

//    uint16_t period = get_aph1_freq()/freq;
    TIMx->ARR = 100;
    TIMx->PSC = (get_aph1_freq()/3000) - 1;
    TIMx->EGR = TIM_EGR_UG;
    uint16_t ccr = dutycycle;
    switch (channel) {
        case TIM_CHANNEL_1:
            TIMx->CCMR1 =(TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1M_2) | (TIM_CCMR1_OC1PE);
            TIMx->CCER = (TIM_CCER_CC1E);
            TIMx->CCR1 = ccr;//* (dutycycle/100);
            TIMx->CR2 = (TIM_CR2_MMS_0);
            TIMx->CR1 = (TIM_CR1_CEN);
            return true;
        case TIM_CHANNEL_2:
            TIMx->CCMR1 =(TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2M_2) | (TIM_CCMR1_OC2PE);
            TIMx->CCR2 = ccr;
            TIMx->CCER = (TIM_CCER_CC2E);
            TIMx->CR2 = (TIM_CR2_MMS_0);
            TIMx->CR1 = TIM_CR1_CEN;
            return true;
        case TIM_CHANNEL_3:
            TIMx->CCMR2 = (TIM_CCMR2_OC3M_1 | TIM_CCMR2_OC3M_2) | (TIM_CCMR2_OC3PE);
            TIMx->CCER |= (TIM_CCER_CC3E);
            TIMx->CCR3 = ccr;
            TIMx->CR2 = (TIM_CR2_MMS_0);
            TIMx->CR1 = TIM_CR1_CEN;
            return true;
        case TIM_CHANNEL_4:
            TIMx->CCMR1 = (TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1M_2);
            TIMx->CCMR2 = (TIM_CCMR2_OC4PE);
            TIMx->CCER = (TIM_CCER_CC4E);
            TIMx->CCR4 = ccr;
            TIMx->CR2 = (TIM_CR2_MMS_0);
            TIMx->CR1 = TIM_CR1_CEN;
            return true;
    }

    return false;
}

