#ifndef _STM_DELAY_H_
#define _STM_DELAY_H_
#include "hw_config.h"
#include <stdint.h>
void stm_msleep(uint16_t ms);
static inline void stm_usleep(uint16_t us)
{
    asm volatile ("MOV R0,%[loops]\n\t"
                "1: \n\t"
                "SUB R0, #1\n\t"
                "CMP R0, #0\n\t"
                "BNE 1b \n\t" : : [loops] "r" (8*us) : "memory"
                );
}

#endif /* end of _STM_DELAY_H_ */

